#include <stdio.h>
#include <string.h>

void encrypt(char *str) {
int i;
for(i = 0; i < strlen(str); i++) {
if(str[i] >= 'a' && str[i] <= 'z') {
str[i] = (str[i] - 'a' + 3) % 26 + 'a';
} else if(str[i] >= 'A' && str[i] <= 'Z') {
str[i] = (str[i] - 'A' + 3) % 26 + 'A';
}
}
}

int main() {
char str[] = "ed25591";
encrypt(str);
printf("Encrypted string: %s\n", str);
return 0;
}
