#define _POSIX_C_SOURCE 200809L

#include <stddef.h>	 /* NULL */
#include <dirent.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <limits.h>
#include <fcntl.h>
#include <libgen.h>


/* Souborové systémy bývají organizovány jako hierarchie souborů
 * a složek. Vaším úkolem je naprogramovat procedury pro vyjádření
 * obsahu podstromu zadaného adresářem.
 *
 * Výstup se bude realizovat skrze strukturu ‹node›, která má
 * následující atributy:
 *  • ‹name› – ukazatel na nulou zakončený řetězec se jménem
 *	souboru;
 *  • ‹type› – hodnota výčtového typu níže popisující typ souboru;
 *  • ‹error› – hodnota ‹0›, nebo informace o nastalé chybě;
 *  • ‹dir› – v případě, že se jedná o adresář, obsahuje ukazatel na
 *	zřetězený seznam souborů, anebo ‹NULL›, pokud je adresář
 *	prázdný;
 *  • ‹next› – ukazatel na další prvek ve stejném adresáři, anebo
 *	‹NULL›, pokud je poslední.
 *
 * Tato struktura reprezentuje jednu položku v adresářovém
 * podstromě. Položka je buď adresářem, nebo jiným souborem. Jelikož
 * se jedná o reprezentaci zřetězeným seznamem, struktura obsahuje
 * ukazatele na následníka a potomka (‹next› a ‹dir›).
 *
 * Atributy ‹name›, ‹dir› a ‹next› jsou ukazatele na dynamicky
 * alokovanou paměť, kterou následně musí být možné uvolnit
 * zavoláním ‹free›.
 *
 * Strukturu nemůžete nijak měnit.
 *
 * Výčtový typ ‹file_type› popisuje pět typů souborů, které budeme
 * rozlišovat: adresář, obyčejný soubor, symbolický odkaz, socket
 * a vše ostatní. */

enum file_type
{
	t_directory,
	t_regular,
	t_symlink,
	t_socket,
	t_other,
};

struct node
{
	char *name;
	enum file_type type;

	int error;

	struct node *dir;
	struct node *next;
};

/* Úkol spočívá v implementaci dvou procedur níže. První z nich je
 * ‹tree_create›, která bere parametry:
 *  • ‹at› – popisovač adresáře, ve kterém hledat složku, nebo
 *	konstanta ‹AT_FDCWD›;
 *  • ‹root_name› – název počátečního adresáře, který se má
 *	prohledávat;
 *  • ‹out› – výstupní ukazatel, do něhož má být uložen ukazatel
 *	na alokovanou zřetězenou strukturu.
 *
 * Její návratovou hodnotou bude:
 *  • ‹0› – úspěch;
 *  • ‹-1› – selhání pří přístupu k některému souboru či složce;
 *  • ‹-2› – selhání při alokaci, což je kritická chyba, a výstupní
 *	‹out› nechť je nastaven na ‹NULL›.
 *
 * Jestliže je návratová hodnota ‹-1›, u dotčených souborů bude
 * atribut ‹error› nastaven na odpovídající hodnotu proměnné
 * ‹errno›. Jinak má být hodnota tohoto atributu ‹0›. */

void tree_free(struct node *tree);

// this function is for debugging purposes only
void print_node(const struct node *node) {
	printf("---------\n");
	printf("name: %s\n", node->name);
	printf("  address: %p\n", node);
	printf("  file type: %d\n", node->type);
	printf("  next node: %p\n", node->next);
	printf("  directory: %p\n", node->dir);
	printf("  error code: %d\n", node->error);
	printf("---------\n");
}

// this function is also just for debugging purposes
void print_tree(const struct node *tree) {
	const struct node *node = tree;
	printf("<+++++++\n");
	printf("Printing parent node\n");
	print_node(node);
	printf("+++++++>\n");

	if (node->dir != NULL) {
		printf("Printing children\n");
		print_tree(node->dir);
	}

	printf("Printing siblings\n");
	node = node->next;
	while( node != NULL) {
		print_tree(node);
		node = node->next;
	}
}

struct node *create_node(char *name) {
	struct node *new_node = malloc(sizeof(struct node));
	if (!new_node) return NULL;

	new_node->name = strdup(name);
	new_node->type = 0;
	new_node->error = 0;
	new_node->dir = NULL;
	new_node->next = NULL;

	return new_node;
}

enum file_type get_file_type(struct stat entry_stat) {
	enum file_type type;
	if (S_ISDIR(entry_stat.st_mode)) {
		type = t_directory;
	} else if (S_ISREG(entry_stat.st_mode)) {
		type = t_regular;
	} else if (S_ISLNK(entry_stat.st_mode)) {
		type = t_symlink;
	} else if (S_ISSOCK(entry_stat.st_mode)) {
		type = t_socket;
	} else {
		type = t_other;
	}
	return type;
}

int build_tree(char *dir_name, struct node **tree) {
//	(*tree)->error = 0;
	int result = 0;

	// save parent to current pointer
	struct node *current = *tree;

//	printf("Processing directory %s\n", dir_name);
	// change dir to start
	DIR *dir = opendir(dir_name);

	if (!dir) {
		(*tree)->error = errno;
		errno = 0;
		return -1;
	}


	int first = 0;

	// Set errno to 0, just in case
	errno = 0;
	struct dirent *entry;

	while (1) {
		errno = 0;
		entry = readdir(dir);
		if (!entry) {
			result = -1;
			break;
		}
		// skip . and ..
		if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) continue;

//		printf("Found file %s\n", entry->d_name);

		// Create node
		struct node *next;
		next = create_node(entry->d_name);
		if (next == NULL) {
			result = -2;
			goto exit;
//			return -2;
		}

		char path[PATH_MAX];
		snprintf(path, PATH_MAX, "%s/%s", dir_name, entry->d_name);

		// Get info about node
		struct stat entry_stat;
		// We cannot get info about node, but we can continue with other node/s
		errno = 0;
		if (stat(path, &entry_stat) < 0) {
			next->error = errno;
			result = -1;
			continue;
		}

		// Get node type
		enum file_type type;
		type = get_file_type(entry_stat);
		next->type = type;


		// If this is the first file in directory, we need to make a link in parent->dir and set current node to parent->dir
		// if not, we need to make a link in current->next and set current node to current->next
		if (first == 0) {
			current->dir = next;
			current = current->dir;
			first = 1;
		} else {
			current->next = next;
			current = current->next;
		}

		// And if the node is directory, we need to call build_tree recursively
		if (type == t_directory) {
			result = build_tree(path, &current);
			if (result == -2) {
				errno = 0;
				goto exit;
//				return -2;
			}
		}
	}

exit:
	// We have received null, there still might be an error processing current directory so we need to check that
	if (errno != 0) {
		(*tree)->error = errno;
		errno = 0;
	}

	// in any case, close the dir
	closedir(dir);
	return result;
}

// Creates tree from directory
int tree_create(int at, char *root_name, struct node **out) {
	*out = create_node(root_name);
	if (!*out) {
		return -2;
	}
	(*out)->type = t_directory;

	if (at != AT_FDCWD) {
		if (fchdir(at) == -1) {
			(*out)->error = errno;
			return -1;
		}
	}

	int result = build_tree(root_name, out);


	// We have already created some nodes, we need to free them
	if (result == -2) {
		tree_free(*out);
	}
	return result;
}


/* Je rovněž potřeba implementovat odpovídající uvolňovací
 * proceduru. Tou je zde ‹tree_free›, která musí být schopna
 * přijmout výstupní ukazatel z ‹tree_create› a ten uvolnit, včetně
 * všech přidělených zdrojů. */


void free_node(struct node *node) {
	if (node == NULL) return;

	free(node->name);
	free_node(node->dir);
	free_node(node->next);

	free(node);
}

void tree_free(struct node *tree) {
	free_node(tree);
}


/* ┄┄┄┄┄┄┄ %< ┄┄┄┄┄┄┄┄┄┄ následují testy ┄┄┄┄┄┄┄┄┄┄ %< ┄┄┄┄┄┄┄ */

#include <assert.h>	 /* assert */
#include <sys/stat.h>   /* mkdirat */
#include <fcntl.h>	  /* mkdirat, openat */
#include <unistd.h>	 /* unlinkat, close */
#include <string.h>	 /* strcmp */
#include <fcntl.h>	  /* unlinkat */
#include <errno.h>	  /* errno */
#include <err.h>		/* err, warn */

static void close_or_warn( int fd, const char *name )
{
	if ( close( fd ) == -1 )
		warn( "closing %s", name );
}

static void unlink_if_exists( int at, const char *name )
{
	if ( unlinkat( at, name, 0 ) == -1 && errno != ENOENT )
		err( 2, "unlinking %s", name );
}

static void rmdir_if_exists( int at, const char *name )
{
	if ( unlinkat( at, name, AT_REMOVEDIR ) == -1 && errno != ENOENT )
		err( 2, "removing directory %s", name );
}

static int open_dir_at( int at, const char *name )
{
	int fd = openat( at, name, O_RDONLY | O_DIRECTORY );

	if ( fd == -1 )
		err( 2, "opening dir %s", name );

	return fd;
}

static void create_file( int at, const char *name )
{
	int fd = openat( at, name, O_CREAT | O_TRUNC | O_WRONLY, 0666 );
	if ( fd == -1 )
		err( 2, "creating %s", name );
	close_or_warn( fd, name );
}

static int create_dir( int at, const char *dir )
{
	rmdir_if_exists( at, dir );

	if ( mkdirat( at, dir, 0755 ) == -1 )
		err( 2, "creating directory %s", dir );

	return open_dir_at( at, dir );
}

int main( void )
{
	unlink_if_exists( AT_FDCWD, "zt.b_root/folder/file_b" );
	rmdir_if_exists(  AT_FDCWD, "zt.b_root/folder" );
	unlink_if_exists( AT_FDCWD, "zt.b_root/file_a" );
	rmdir_if_exists(  AT_FDCWD, "zt.b_root" );

	int root = create_dir( AT_FDCWD, "zt.b_root" );
	int folder = create_dir( root, "folder" );
	create_file( root, "file_a" );
	create_file( folder, "file_b" );
	close_or_warn( folder, "folder" );

	struct node *tree;
	tree_create( AT_FDCWD, "zt.b_root", &tree );

//	printf("Printing tree:\n");	// smfsakfsdaklghjaslkgdfklhjfdkggafg
//	print_tree(tree);			// fkajsdflkjasdůklfjasdlfkjasdkljfgdgdfgdf

	assert( tree->type == t_directory );
	assert( tree->dir != NULL );
//	printf( "%p\n", tree->next );
	assert( tree->next == NULL );
	assert( strcmp( tree->name, "zt.b_root" ) == 0 );
	assert( tree->dir->next != NULL );
	assert( tree->dir->next->next == NULL );

	assert( tree->dir->type == t_directory
		 || tree->dir->next->type == t_directory );

	const struct node *tree_folder;
	const struct node *tree_file;
	if ( tree->dir->type == t_directory )
	{
		tree_folder = tree->dir;
		tree_file = tree->dir->next;
	}
	else
	{
		tree_folder = tree->dir->next;
		tree_file = tree->dir;
	}

//	printf("\n\n\nPrinting second tree:\n");
//	print_tree(tree_folder);

	assert( strcmp( tree_folder->name, "folder" ) == 0 );
	assert( strcmp( tree_file->name, "file_a" ) == 0 );
	assert( tree_folder->type == t_directory );
	assert( tree_folder->dir != NULL );
//	printf("%s\n", tree_folder->dir->name);
	assert( strcmp( tree_folder->dir->name, "file_b" ) == 0 );

	tree_free( tree );

	close_or_warn( root, "zt.b_root" );

	unlink_if_exists( AT_FDCWD, "zt.b_root/folder/file_b" );
	rmdir_if_exists(  AT_FDCWD, "zt.b_root/folder" );
	unlink_if_exists( AT_FDCWD, "zt.b_root/file_a" );
	rmdir_if_exists(  AT_FDCWD, "zt.b_root" );
	return 0;
}
