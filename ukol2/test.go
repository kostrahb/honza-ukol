package main

import(
	"os"
	"fmt"
	"bufio"
	"time"
)


func main() {
	// various shenanigans with waiting for output, writing both directly to stdout and stderr and buffering and flushing content

	// Prepare buffered output to stdout and stderr
	stdout := bufio.NewWriter(os.Stdout)
	stderr := bufio.NewWriter(os.Stderr)

	// test repeated write without new line
	stdout.Write([]byte("asdf"))
	stdout.Flush() // Wtf? Why this flush does not make pipe in b_combine flush??!
	stdout.Write([]byte("asdf"))
	stdout.Flush()
	// here we add sleep to make the pipe in b_combine flush (don't ask why, but it breaks stdout read in b_combine)
	time.Sleep(1 * time.Second)
	stdout.Write([]byte("asdf"))
	stdout.Flush()

	// Start writing to stdout
	stdout.Write([]byte("asdf\n"))
	stdout.Write([]byte("asdf\n"))

	// meanwhile add some error output and flush error output first
	stderr.Write([]byte("qwer\n"))
	stderr.Write([]byte("qwer\n"))
	stdout.Write([]byte("asdf\n"))
	stderr.Flush()

	// some more error output after a pause
	time.Sleep(1 * time.Second)
	stderr.Write([]byte("qwer\n"))
	stderr.Write([]byte("qwer\n"))
	stderr.Flush()
	stderr.Write([]byte("qwer\n"))
	stderr.Write([]byte("qwer\n"))
	stderr.Flush()
	stdout.Flush()

	// again a pause and more error and normal output
	time.Sleep(1 * time.Second)
	stderr.Write([]byte("qwer1\n"))
	stderr.Write([]byte("qwer1\n"))
	stderr.Write([]byte("qwer1\n"))
	time.Sleep(1 * time.Second)
	stdout.Write([]byte("asdf1\n"))
	stdout.Write([]byte("asdf1\n"))
	stdout.Write([]byte("asdf1\n"))
	time.Sleep(1 * time.Second)

	// Add some non-buffered writes into the mixture (it should go first before the asdf1 stdout qwer1 stderr output)
	fmt.Fprintf(os.Stderr, "your message 1 here\n");
	fmt.Fprintf(os.Stdout, "your message 1 here\n");
	stdout.Flush()
	time.Sleep(1 * time.Second)
	stderr.Flush()
	stdout.Write([]byte("asdf1\n"))
	stdout.Write([]byte("asdf1\n"))
	stdout.Write([]byte("asdf1\n"))

	stdout.Flush()
	stderr.Flush()
	fmt.Fprintf(os.Stderr, "your message 3 here\n");
	fmt.Fprintf(os.Stdout, "your message 4 here\n");
	fmt.Fprintf(os.Stderr, "your message 5 here\n");
	fmt.Fprintf(os.Stderr, "your message 6 here\n");
	fmt.Fprintf(os.Stderr, "your message 7 here\n");

	// test correct handling of long message from stdout when there is no error output
	fmt.Fprintf(os.Stdout, "your message 4 here\n");
	fmt.Fprintf(os.Stdout, "your message 4 here\n");
	fmt.Fprintf(os.Stdout, "your message 4 here\n");
	fmt.Fprintf(os.Stdout, "your message 4 here\n");
	fmt.Fprintf(os.Stdout, "your message 4 here\n");
	fmt.Fprintf(os.Stdout, "your message 4 here\n");
	fmt.Fprintf(os.Stdout, "your message 4 here\n");
	fmt.Fprintf(os.Stdout, "your message 4 here\n");
	fmt.Fprintf(os.Stdout, "your message 4 here\n");
	fmt.Fprintf(os.Stdout, "your message 4 here\n");
	fmt.Fprintf(os.Stdout, "your message 4 here\n");
}
